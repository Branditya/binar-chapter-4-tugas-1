package id.branditya.binarchapter4tugas1

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.Toast
import androidx.annotation.LayoutRes
import com.google.android.material.snackbar.Snackbar
import id.branditya.binarchapter4tugas1.databinding.FragmentFirstBinding
class FirstFragment : Fragment() {
    private var _binding : FragmentFirstBinding? = null
    private val binding get() = _binding!!

    /**
     * Adds an extra action button to this snackbar.
     * [aLayoutId] must be a layout with a Button as root element.
     * [aLabel] defines new button label string.
     * [aListener] handles our new button click event.
     */
    private fun Snackbar.addAction(@LayoutRes aLayoutId: Int, aLabel: String, aListener: View.OnClickListener?) : Snackbar {
        // Add our button
        val button = LayoutInflater.from(view.context).inflate(aLayoutId, null) as Button
        // Using our special knowledge of the snackbar action button id we can hook our extra button next to it
        view.findViewById<Button>(com.google.android.material.R.id.snackbar_action).let {
            // Copy layout
            button.layoutParams = it.layoutParams
            // Copy colors
            (button as? Button)?.setTextColor(it.textColors)
            (it.parent as? ViewGroup)?.addView(button)
        }
        button.text = aLabel
        /** Ideally we should use [Snackbar.dispatchDismiss] instead of [Snackbar.dismiss] though that should do for now */
        //extraView.setOnClickListener {this.dispatchDismiss(BaseCallback.DISMISS_EVENT_ACTION); aListener?.onClick(it)}
        button.setOnClickListener {this.dismiss(); aListener?.onClick(it)}
        return this
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentFirstBinding.inflate(inflater,container,false)
        return binding.root
    }

    override fun onDestroy() {
        super.onDestroy()
        _binding = null
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        btnToastPressed()
        btnSnackbarPressed()
    }

    private fun btnToastPressed() {
        binding.btnToast.setOnClickListener {
            val name = binding.etName.text.toString()
            Toast.makeText(requireContext(), "Selamat Datang $name",Toast.LENGTH_LONG).show()
        }
    }

    private fun btnSnackbarPressed() {
        binding.btnSnackbar.setOnClickListener {
            Snackbar.make(it, "Pilih Aksi", Snackbar.LENGTH_INDEFINITE).setAction("Action 1") {
                Toast.makeText(requireContext(),"Aksi pertama dijalankan", Toast.LENGTH_LONG).show()
            }.addAction(R.layout.snackbar_extra_button, "Action 2"){
                Toast.makeText(requireContext(),"Aksi kedua dijalankan", Toast.LENGTH_LONG).show()
            }.show()
        }
    }
}